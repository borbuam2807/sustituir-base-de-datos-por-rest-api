from flask import Flask, request
from flask_restful import Api, Resource

app = Flask(__name__)
api = Api(app)

# Simulación de datos
users = []

class UserResource(Resource):
    def get(self, user_id):
        user = next((user for user in users if user['id'] == user_id), None)
        if user:
            return user, 200
        return "Usuario no encontrado", 404

    def post(self):
        new_user = request.get_json()
        new_user['id'] = len(users) + 1
        users.append(new_user)
        return new_user, 201

api.add_resource(UserResource, '/users/<int:user_id>', '/users')

if __name__ == '__main__':
    app.run()
