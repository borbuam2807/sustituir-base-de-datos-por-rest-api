from flask import Flask, render_template
import requests

app = Flask(__name__)

API_BASE_URL = "http://127.0.0.1:5000"  # Cambia esto a la URL de tu API

@app.route('/')
def index():
    return '¡Bienvenido a la aplicación Flask con API REST!'

@app.route('/user/<int:user_id>')
def user_profile(user_id):
    response = requests.get(f"{API_BASE_URL}/users/{user_id}")
    if response.status_code == 200:
        user = response.json()
        return f"ID: {user['id']}, Username: {user['username']}, Email: {user['email']}"
    return "Usuario no encontrado", 404

if __name__ == '__main__':
    app.run()
